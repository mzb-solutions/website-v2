# v0.1 - 18/01/2020
initial commit/push/test/deployment success :satisfied:

Changes (only the obvious stuff):
- TODO(smzb): 
  1. ~~Create DEBUG flag in drone and use in test script~~ Simply used the JEKYLL_ENV flag that already exists. duh!
  2. Sanitize the drone pipelines, in terms of:
    - ~~when to stage and when to only do a dry build without deply, even though we are on a pull request~~
    - split pipelines, master/push for deploy pr for staging and dev->test ~~(@smzb : do we reeeaaaally need this?)~~ see #10 
    - In future: change domain depending on above circumstance(s) ~~(@smzb: lets make MS out of this, or at least a feature)~~ see #11
- Added Documentation for Test Script
  - TODO: Add more sophisticated means of documentation with the obvious candidates (ie: netlify.com/readthedocs.io or go the path of mkdocs and the likes) see #12
- Added VolumeCache to store the gems during build for later reuse (unless of course they're outdated)
