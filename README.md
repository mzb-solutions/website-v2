# Website-V2

A jekyll-built static website, hosted through some netlify-like (most likely) or similar platform

We are intending to use a self hosted drone.io instance to test and build this website

## Build Status

[![Build Status](https://ci.mzb.company/api/badges/MzB-Solutions/website-v2/status.svg?ref=refs/heads/dev)](https://ci.mzb.company/MzB-Solutions/website-v2)

## Deploy Status

[![Build Status](https://ci.mzb.company/api/badges/MzB-Solutions/website-v2/status.svg?ref=refs/heads/master)](https://ci.mzb.company/MzB-Solutions/website-v2)

# How to contribute

## Remote Development

Clone and checkout this repository on the dev branch, make any changes necessary, and then simply commit those changes back upstream

After that open a [PR](https://code.mzb.company/mzb-solutions/website-v2/compare/master...dev)

### A note about the gem cache
We've added drone-volume-cache to store the gems during build for later reuse (unless of course they're outdated and/or purged)
  This can be influenced through certain COMMIT_MSG triggers: [CLEAR CACHE] and [NO CACHE]
If your commit message contains one of the above keywords, the volume cache for /usr/local/bundle will be purged/not used.

## Local Development

Ensure you have Ruby 2.7+ and Bundler installed.

to test your local installation :

``` shell
bundle exec jekyll serve
```
You can close that server instance by pressing `CTRL+C`

## Notes/Known issues:
- In most cases it should be enough to participate in remote development without having to install all pre-reqs
- The pipeline configuration took a lot of doing to get it working, so please bear that in mind before you put your grubby little fingers on it. *Talking to you, [@smzb]*
