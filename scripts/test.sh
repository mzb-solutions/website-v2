#!/bin/bash
### cd into current workspace
pushd $CI_WORKSPACE_BASE
### Set env vars to look for null/dot files/dirs during glob operations
shopt -s nullglob
shopt -s dotglob
# Die if no dir name provided on command line
[[ $# -eq 0 ]] && { echo "Usage: $0 directory"; exit 1; }
### What are we checking? and sanitize by appending glob
chk_files=(${1}/*)
### bash'ism for if exists
(( ${#chk_files[*]} )) && not_found=0 || not_found=1
### Unset above env vars
shopt -u nullglob
shopt -u dotglob
### Pop the current workspace of the dir stack and jump back to old location
popd
### Output result (when in debug)
if [ $JEKYLL_ENV == "development" ]
then
  if [ $not_found == 1 ] 
  then
      echo "Jekyll build failed!! [$CI_WORKSPACE_BASE/$1] is empty or non-existing!"
  else
      echo "Jekyll build succeeded, directory [$CI_WORKSPACE_BASE/$1] contains the files necessary."
  fi
fi
### Exit with non-zero if not found so CI can pick up from here...
exit $not_found
